from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.process import pm


inf_sun_points = AllocatingCodeInjection(
    pm.base_address + 0x1F636,
    """
        mov dword [edi + 0x5578], 9999
    """,
    original_code_length=6,
)

no_cooldown = CodeInjection(pm.base_address + 0x958C5, "nop\n" * 2)
