from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.tapper import TapperUI

from injections import inf_sun_points, no_cooldown


@simple_trainerbase_menu("Plants vs. Zombies", 450, 300)
def run_menu():
    add_components(
        CodeInjectionUI(inf_sun_points, "Infinite Sun Points", "F1"),
        CodeInjectionUI(no_cooldown, "No Cooldown", "F2"),
        SeparatorUI(),
        TapperUI(),
    )
